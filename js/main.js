var controller;
if (Modernizr.touch) {
  var myScroll;
  $(document).ready(function () {
    // wrap for isroll
    $("#content-wrapper")
      .addClass("scrollContainer")
      .wrapInner('<div class="scrollContent"></div>');

    // add iScroll
    myScroll = new IScroll('#content-wrapper', {scrollX: false, scrollY: true, scrollbars: true, useTransform: false, useTransition: false, probeType: 3});
  
    // update container on scroll
  myScroll.on("scroll", function () {
    controller.update();
  });

  // overwrite scroll position calculation to use child's offset instead of parents scrollTop();
  controller.scrollPos(function () {
    return -myScroll.y;
  });

  // refresh height, so all is included.
  setTimeout(function () {
      myScroll.refresh();
  }, 0);

    $("#content-wrapper").on("touchend", "a", function (e) {
      // a bit dirty workaround for links not working in iscroll for some reason...
      e.preventDefault();
      window.location.href = $(this).attr("href");
    });

  // manual set hight (so height 100% is available within scroll container)
    $(document).on("orientationchange", function () {
      $("section")
        .css("min-height", $(window).height())
        .parent(".scrollmagic-pin-spacer").css("min-height", $(window).height());
    });
    $(document).trigger("orientationchange"); // trigger to init
  });
  // init the controller
  controller = new ScrollMagic({
    container: "#content-wrapper",
    globalSceneOptions: {
      triggerHook: "onLeave"
    }
  });
} else {
  // init the controller
  controller = new ScrollMagic({
    globalSceneOptions: {
      triggerHook: "onLeave"
    }
  });
}

$(document).ready(function() {
  loaderTexts = new Array("Загружаю выставку", "Загрузка может занять продолжительное время", "Пожалуйста, подождите");
  textsCount = textNumber =loaderTexts.length;

  changeLoaderText = function() {
    if ($("#loader-screen").css("display") === "block") {
      textNumber++;
      newText = loaderTexts[textNumber % textsCount]
      console.log("Loader text changed to: " + newText);
      $("#loader-text").fadeOut(500, function() {
        $("#loader-text").text(newText).fadeIn(500);
      });
      setTimeout(changeLoaderText, 5000);
    }
  };
  setTimeout(changeLoaderText, 5000);

  // Загрузочный экран убирается, когда загружаются фоновые изображения
  loadBg = function(src, block, onFinish) {
    $('<img/>').attr('src', src).load(function() {
      $(block).css('background-image', function() {
        if (onFinish) onFinish();
        return 'url("' + src + '")'
      });
      // console.log("Background image " + src + " loaded to block " + block);
    });
  };

  $(window).load(function() {
    loadBg('img/docs/001_xxxl.jpg', '#part2-photo1');
    loadBg('img/docs/002_xxxl.jpg', '#part2-photo2');
    loadBg('img/docs/016_xxxl.jpg', '#part16-photo1', function() {
      $("#loader-screen").fadeOut("500");
    });
  });
  
});